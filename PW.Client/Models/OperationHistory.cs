﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.DAL.Views
{
    public class OperationHistory
    {
        public UserHistory RecipientUser { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Balance { get; set; }
    }
}