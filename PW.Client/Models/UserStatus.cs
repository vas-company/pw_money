﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.DAL.Views
{
    public class UserStatus
    {
        public string Name { get; set;}
        public string Email { get; set; }
        public decimal Balance { get; set; }
    }
}