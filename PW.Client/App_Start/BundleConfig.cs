﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace PW.Client.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

         
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                       "~/Content/Site.css",
                       "~/Content/autocomplete.css"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                       "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-route.js",
                  "~/Scripts/angular-cookies.js",
                 "~/Scripts/angular-mocks.js",
                 "~/Scripts/autocomplete.js"));

            bundles.Add(new ScriptBundle("~/bundles/smarttable").Include(
               "~/Scripts/smart-table.js"));

            bundles.Add(new ScriptBundle("~/bundles/scriptsng").Include(
                       "~/ScriptsNG/Controllers/*.js",
                       "~/ScriptsNG/Services/*.js",
                       "~/ScriptsNG/Factories/*.js",
                       "~/ScriptsNG/ClientAngular.js"));
        }
    }
}