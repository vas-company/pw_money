﻿var SessionService = function ($cookies) {
    var userinfo =
        {
            name: "...",
            balance: 0
        }

    this.setName = function (name) {
        $cookies.put('clientName', name);
    }

    this.setBalance = function (balance) {
        $cookies.put('clientBalance', balance);
    }

    this.getName = function () {
        if (!$cookies.get('clientName')) {
            return undefined;
        }
        return $cookies.get('clientName');
    }

    this.getBalance = function () {
        if (!$cookies.get('clientBalance')) {
            return undefined;
        }
        return $cookies.get('clientBalance');
    }

    this.getToken = function () {
        if (!$cookies.get('clientAngularToken')) {
                return undefined;
        }
        return $cookies.get('clientAngularToken');
    }

    this.setToken = function (token) {
        $cookies.put('clientAngularToken', token) ;
    }


    this.apiUrl = 'http://localhost:50207';

}
SessionService.$inject = ['$cookies'];