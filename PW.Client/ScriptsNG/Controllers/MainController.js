﻿var MainController = function ($scope,$timeout, OperationFactory, UserFactory, SessionService) {
    $scope.mainForm = {
        recipientName: null,
        amount: null,
        errorMessage: undefined,
        operations: [],
        displayOperations: [],
        autoUsers: [],
        isLoading: false,
        modelState: undefined
    };
    function loadingStart(){
        $scope.mainForm.isLoading = true;
    }
    function loadingEnd() {
        $scope.mainForm.isLoading = false;
    }

    function setError(response) {
        if (response.modelState != undefined) {
            $scope.mainForm.modelState = [];
            for (var key in response.modelState) {
                for (var i = 0; i < response.modelState[key].length; i++) {
                    $scope.mainForm.modelState.push(response.modelState[key][i]);
                }
            }
            return;
        }
        if (response.error_description != undefined) {
            $scope.mainForm.errorMessage = response.error_description;
            return;
        }
        if (response.message != undefined) {
            $scope.mainForm.errorMessage = response.message;
            return;
        }
    }
    function dropError() {
        $scope.mainForm.errorMessage = undefined;
        $scope.mainForm.modelState = undefined;
    }
    $scope.updateOperations = function () {
        OperationFactory.history()
         .then(function (response) {
             $scope.mainForm.operations = response;
         }, function (response) {
             setError(response);
         });
    }
    $scope.createOperations = function () {
        dropError();
        loadingStart();
        OperationFactory.create($scope.mainForm.amount, $scope.mainForm.recipientName)
         .then(function (response) {
             $scope.updateOperations();
             $scope.updateBalance();
             loadingEnd();
         }, function (response) {
             loadingEnd();
             setError(response);
         });
    }
    $scope.updateUsers = function () {
        UserFactory.getAll()
          .then(function (response) {
              $scope.mainForm.autoUsers= [];
              var user={};
              for (var i = 0; i <  response.length; i++) {       
                  user=response[i];
                  $scope.mainForm.autoUsers.push({ value: user.userName, label: user.userName+' ('+user.email+')'});
              }
          }, function (response) {
              setError(response);
          });
    }

    $scope.selectUser = {};
    $scope.selectHandler = function (selected, selectUser) {
        if (selected) {
            $scope.mainForm.recipientName = selectUser.value;
        }
    }

    $scope.updateBalance = function () {
        UserFactory.getBalance()
       .then(function (response) {
           SessionService.setBalance(response.balance);
       }, function (response) {
           setError(response);
       });
    }

    function startUpdateBalance () {
        $scope.updateBalance();
        $timeout(startUpdateBalance, 15000);
    }
    $scope.init = function () {
        $scope.updateUsers();
        $scope.updateOperations();
       startUpdateBalance();
    }

    $scope.historySelected = function (row) {
        //если я не отправитель, значит перевод сделали мне
        if (row.isSender) {
            $scope.mainForm.recipientName = row.recipientName;
        }
        else {
            $scope.mainForm.recipientName = row.senderName;            
        }
        //$scope.selectUser = $scope.mainForm.recipientName;
        $scope.mainForm.amount = row.amount;
    }
}

MainController.$inject = ['$scope', '$timeout', 'OperationFactory', 'UserFactory', 'SessionService'];

