﻿var BaseController = function ($scope, SessionService) {
     $scope.loggedIn = function () {
         return SessionService.getToken() !== undefined;
     }
     $scope.name = function () {
         return SessionService.getName();
     }
     $scope.balance = function () {
         return SessionService.getBalance();
     }
}
BaseController.$inject = ['$scope', 'SessionService'];

function ParamToUrlRow(obj) {
    var str = [];
    var s = "";
    var value=null;
    for (var p in obj) {
        value=obj[p];
        s=encodeURIComponent(p) + "=";
        if (value != undefined) {
            s=s+encodeURIComponent(value);
        }
        str.push(s);
    }
    return str.join("&");
}