﻿var LoginController = function ($scope, $location, SessionFactory, SessionService) {
    $scope.loginForm = {
        username: undefined,
        password: undefined,
        errorMessage: undefined,
        isLoading: false
    };
    function loadingStart() {
        $scope.loginForm.isLoading = true;
    }
    function loadingEnd() {
        $scope.loginForm.isLoading = false;
    }
    $scope.login = function () {
        loadingStart();
        SessionFactory.create($scope.loginForm.username, $scope.loginForm.password)
        .then(function (response) {
            SessionService.setToken(response.access_token);
            SessionService.setName(response.user_name);
            $scope.loginForm.isLoading = false;
            $location.path('/main');
        }, function (response) {
            $scope.loginForm.isLoading = false;
            $scope.loginForm.errorMessage = response.error_description;
        });
    }

    $scope.logout = function () {
        loadingStart();
        SessionFactory.delete()
        .then(function (response) {
            SessionService.setToken(undefined);
            loadingEnd();
            $location.path('/login');
        }, function (response) {
            SessionService.setToken(undefined);
            loadingEnd();
            $scope.indexError.errorMessage = response.message;
        });
    }
}
LoginController.$inject = ['$scope', '$location', 'SessionFactory', 'SessionService'];