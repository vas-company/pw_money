﻿var RegisterController = function ($scope, $location, SessionFactory, UserFactory, SessionService) {
    $scope.registerForm = {
        username: undefined,
        email: undefined,
        password: undefined,
        confirmPassword: undefined,
        errorMessage: undefined,
        isLoading: false
    };
    function loadingStart() {
        $scope.registerForm.isLoading = true;
    }
    function loadingEnd() {
        $scope.registerForm.isLoading = false;
    }
   function setError(response) {
        if (response.modelState != undefined) {
            $scope.registerForm.modelState = [];
            for (var key in response.modelState) {
                for (var i = 0; i < response.modelState[key].length; i++) {
                    $scope.registerForm.modelState.push(response.modelState[key][i]);
                }
            }
            return;
        }
        if (response.error_description != undefined) {
            $scope.registerForm.errorMessage = response.error_description;
            return;
        }
        if (response.message != undefined) {
            $scope.registerForm.errorMessage = response.message;
            return;
        }        
    }
    function dropError() {
        $scope.registerForm.errorMessage = undefined;
        $scope.registerForm.modelState = undefined;
    }
    $scope.register = function () {
        dropError();
        loadingStart();
        if ($scope.registerForm.password != $scope.registerForm.confirmPassword) {
            var response = { "modelState": { "password": ["Пароль не совпадает"] } };
            loadingEnd();
            setError(response);
            return;
        }
        UserFactory.create($scope.registerForm.username, $scope.registerForm.email, $scope.registerForm.password, $scope.registerForm.confirmPassword)
        .then(function () {
            SessionFactory.create($scope.registerForm.username, $scope.registerForm.password)
            .then(function (response) {
                SessionService.setToken(response.access_token);
                SessionService.setName(response.user_name);
                loadingEnd();
                $location.path('/main');
            }, function (response) {
                loadingEnd();
                setError(response);
            });
        }, function (response) {
            loadingEnd();
            setError(response);
        });
    }
}
RegisterController.$inject = ['$scope', '$location', 'SessionFactory', 'UserFactory', 'SessionService'];