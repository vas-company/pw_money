﻿var OperationFactory = function ($http, $q, SessionService) {
    return new function () {
        this.history = function () {
            var result = $q.defer();
            $http({
                method: 'GET',
                url: SessionService.apiUrl + '/api/operation',
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + SessionService.getToken() }
            })
            .success(function (response) {
                result.resolve(response);
            })
            .error(function (response) {
                result.reject(response);
            });

            return result.promise;
        }

        this.create = function (amount, recipientName) {
            var result = $q.defer();
            var params = { amount: amount, recipientName: recipientName };
            $http({
                method: 'POST',
                url: SessionService.apiUrl + '/api/operation?' + ParamToUrlRow(params),
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + SessionService.getToken() }
            })
            .success(function (response) {
                result.resolve(response);
            })
            .error(function (response) {
                result.reject(response);
            });

            return result.promise;
        }
    }
}

OperationFactory.$inject = ['$http', '$q', 'SessionService'];