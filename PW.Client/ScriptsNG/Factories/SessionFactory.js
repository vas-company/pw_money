﻿var SessionFactory = function ($http, $q, SessionService) {
    return new function () {
        this.create =  function (username, password) {
            var result = $q.defer();

            var params = { grant_type: "password", username: username, password: password };

            $http({
                method: 'POST',
                url: SessionService.apiUrl + '/api/session/token',
                transformRequest: function (obj) { return ParamToUrlRow(obj) },
                data: params,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded;'
                }
            })
            .success(function (response) {
                result.resolve(response);
            })
            .error(function (response) {
                result.reject(response);
            });

            return result.promise;
        }

        this.delete = function () {
            var result = $q.defer();
            $http({
                method: 'DELETE',
                url: SessionService.apiUrl + '/api/session',
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + SessionService.getToken() }
            })
            .success(function (response) {
            result.resolve(response);
            })
            .error(function (response) {
                result.reject(response);
            });

            return result.promise;
        }
    }
}

SessionFactory.$inject = ['$http', '$q', 'SessionService'];