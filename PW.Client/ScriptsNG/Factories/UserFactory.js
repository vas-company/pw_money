﻿var UserFactory = function ($http, $q, SessionService) {
    return new function () {

        this.create = function (username, email, password) {
            var result = $q.defer();

            var params = { username: username, email: email, password: password };

            $http({
                method: 'POST',
                url: SessionService.apiUrl + '/api/user?' + ParamToUrlRow(params),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .success(function (response) {
                result.resolve(response);
            })
            .error(function (response) {
                result.reject(response);
            });

            return result.promise;
        }

        this.getAll = function () {
            var result = $q.defer();
            $http({
                method: 'GET',
                url: SessionService.apiUrl + '/api/user',
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + SessionService.getToken() }
            })
            .success(function (response) {
                result.resolve(response);
            })
            .error(function (response) {
                result.reject(response);
            });

            return result.promise;
        }

        this.getBalance = function () {
            var result = $q.defer();
            $http({
                method: 'GET',
                url: SessionService.apiUrl + '/api/user/balance',
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + SessionService.getToken() }
            })
            .success(function (response) {
                result.resolve(response);
            })
            .error(function (response) {
                result.reject(response);
            });

            return result.promise;
        }

    }
}

UserFactory.$inject = ['$http', '$q', 'SessionService'];