﻿var ClientAngular = angular.module('ClientAngular', ['ngRoute', 'ngCookies', 'smart-table', 'auto-complete-module']);

ClientAngular.service('SessionService', SessionService);
ClientAngular.controller('BaseController', BaseController);
ClientAngular.controller('LoginController', LoginController);
ClientAngular.controller('RegisterController', RegisterController);
ClientAngular.controller('MainController', MainController);
ClientAngular.factory('SessionFactory', SessionFactory);
ClientAngular.factory('UserFactory', UserFactory);
ClientAngular.factory('OperationFactory', OperationFactory);

var ConfigFunction = function ($routeProvider) {
    $routeProvider
     .when('/login', {
         templateUrl: 'work/login',
         controller: 'LoginController'
     })
    .when('/register', {
        templateUrl: 'work/register',
        controller: 'RegisterController'
    })     
     .when('/main', {
         templateUrl: 'work/main',
         controller: 'MainController'
     });
};
ConfigFunction.$inject = ['$routeProvider'];
ClientAngular.config(ConfigFunction);

ClientAngular.directive('stSelectRowEx', ['stConfig', function (stConfig) {
    return {
        restrict: 'A',
        require: '^stTable',
        scope: {
            row: '=stSelectRowEx',
            callback: '&stSelected'
        },
        link: function (scope, element, attr, ctrl) {
            var mode = attr.stSelectMode || stConfig.select.mode;
            element.bind('click', function () {
                scope.$apply(function () {
                    ctrl.select(scope.row, mode);
                    scope.callback(scope.row);
                });
            });

            scope.$watch('row.isSelected', function (newValue) {
                if (newValue === true) {
                    element.addClass(stConfig.select.selectedClass);
                } else {
                    element.removeClass(stConfig.select.selectedClass);
                }
            });
        }
    };
}]);