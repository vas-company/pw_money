﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PW.Client.Controllers
{
    public class WorkController : Controller
    {
        // GET: Index
        public ActionResult Index()
        {
            return View();
        }

        // GET: Login
        public PartialViewResult Login()
        {
            return PartialView();
        }

        // GET: Register
        public PartialViewResult Register()
        {
            return PartialView();
        }

        // GET: Main
        public PartialViewResult Main()
        {
            return PartialView();
        }
    }
}