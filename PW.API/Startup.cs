﻿using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using PW.App_Start;
using PW.DAL;
using PW.DAL.Identity;
using System;
using System.Reflection;
using System.Web.Http;

[assembly: OwinStartup(typeof(Startup))]

namespace PW.App_Start
{
    public partial class Startup
    {
        public const string TokenEndpointPath = "/api/session/token";

        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            var container = AutofacConfig.Register(app, config);
            AuthOwinConfig.Register(app);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
        }


    }
}