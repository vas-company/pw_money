﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using PW.DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.App_Start
{
    public class AuthOwinConfig
    {
        public static void Register(IAppBuilder app)
        {
            app.CreatePerOwinContext(AppContext.Create);
            app.CreatePerOwinContext<AuthUserManager>(AuthUserManager.Create);
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString(Startup.TokenEndpointPath),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new OAuthProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
            {
                Provider =new OAuthBearerProvider()
            });

        }
    }
}