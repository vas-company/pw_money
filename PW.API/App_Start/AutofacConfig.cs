﻿using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using PW.DAL;
using PW.DAL.Identity;
using PW.DAL.Models;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace PW.App_Start
{
    public class AutofacConfig
    {
        public static IContainer Register(IAppBuilder app, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            RegisterMainTypes(builder);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            return container;
        }

        public static void RegisterMainTypes(ContainerBuilder builder)
        {
            builder.RegisterType<AuthUserManager>().AsSelf().InstancePerRequest();
            builder.RegisterType<AppContext>().AsSelf().InstancePerRequest();
            builder.RegisterType<Api>().AsSelf().InstancePerRequest();
            builder.Register(c => new UserStore<User>(c.Resolve<AppContext>())).AsImplementedInterfaces().InstancePerRequest();
            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).As<IAuthenticationManager>();
            builder.Register(c => new IdentityFactoryOptions<AuthUserManager>
            {
                DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Application​")
            });
            builder.Register(c => new Repository(c.Resolve<AppContext>(), AuthUserManager.Create(c.Resolve<AppContext>()))).InstancePerRequest();
            //builder.Register(c => new Api(new Repository(
            //    c.Resolve<AppContext>(),
            //    c.Resolve<AuthUserManager>())))
            //    .InstancePerRequest();
        }
    }
}