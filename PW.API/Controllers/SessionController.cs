﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PW.DAL;
using PW.DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PW.Controllers.API
{
    [Authorize]
    [RoutePrefix("api/session")]
    public class SessionController : BaseController
    {
        public SessionController(Api api)
        {
            API = api;
        }

        private Api API { get; set; }

        [HttpDelete]
        [Route("")]
        public async Task<IHttpActionResult> Delete()
        {
            try
            {
                var result = await API.Session.UpdateSecurityStampAsync(User.Identity.GetUserId());
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                return Ok();
            }
            catch (Exception e)
            {
                //TODO: log here                                
                return GetErrorResult(e.Message);
            }
        }
    }
}