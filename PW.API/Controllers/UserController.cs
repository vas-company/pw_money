﻿using Microsoft.AspNet.Identity;
using PW.DAL;
using PW.DAL.Bindings;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace PW.Controllers.API
{
    [Authorize]
    [RoutePrefix("api/user")]
    public class UserController : BaseController
    {
        public UserController(Api api)
        {
            API = api;
        }

        private Api API { get; set; }

        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Create([FromUri]UserCreateIn model)
        {
            try
            {

                if (model == null)
                {
                    return GetErrorResult("Пустой запрос");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await API.Users.CreateAsync(model);
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                return Ok();
            }
            catch (Exception e)
            {
                //TODO: log here                                
                return GetErrorResult(e.Message);
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetAll()
        {
            try
            {
                var result = await API.Users.GetAllAsync();
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                return Ok(result.Value);
            }
            catch (Exception e)
            {
                //TODO: log here                                
                return GetErrorResult(e.Message);
            }
        }

        [HttpGet]
        [Route("balance")]
        public async Task<IHttpActionResult> GetBalance()
        {
            try
            {
                var result = await API.Users.GetBalanceAsync(User.Identity.GetUserId());
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                return Ok(result.Value);
            }
            catch (Exception e)
            {
                //TODO: log here                                
                return GetErrorResult(e.Message);
            }
        }

    }
}