﻿using Microsoft.AspNet.Identity;
using PW.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace PW.Controllers
{
    public class BaseController : ApiController
    {
        protected IHttpActionResult GetErrorResult(IResultError result)
        {   
            string message = result.Errors.First();
            return Content(System.Net.HttpStatusCode.BadRequest, new { message });

        }

        protected IHttpActionResult GetErrorResult(string message)
        {
            return Content(System.Net.HttpStatusCode.BadRequest, new { message });
        }
    }
}