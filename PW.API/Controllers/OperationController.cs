﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PW.DAL;
using PW.DAL.Bindings;
using PW.DAL.Identity;
using PW.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PW.Controllers.API
{
    [Authorize]
    [RoutePrefix("api/operation")]
    public class OperationController : BaseController
    {
        public OperationController(Api api)
        {
            API = api;
        }
        private Api API { get; set; }

        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Create([FromUri]OperationCreateIn model)
        {
            try
            {
                if (model == null)
                {
                    return GetErrorResult("Пустой запрос");
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var id = User.Identity.GetUserId();
                var result = await API.Operation.CreateAsync(id, model);
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                return Ok();
            }
            catch (Exception e)
            {
                //TODO: log here                                
                return GetErrorResult(e.Message);
            }

        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> History()
        {
            try
            {
                var id = User.Identity.GetUserId();
                var result = await API.Operation.FindByIdAsync(id);
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                return Ok(result.Value);
            }
            catch (Exception e)
            {
                //TODO: log here                                
                return GetErrorResult(e.Message);
            }
        }
    }
}