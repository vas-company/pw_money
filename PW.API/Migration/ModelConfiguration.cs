﻿
using PW.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace PW.Migration
{
    public class OperationConfiguration : EntityTypeConfiguration<Operation>
    {
        public OperationConfiguration()
        {
            this.Property(p => p.Date)
                 .HasColumnType("datetime2")
                 .IsRequired();
        }
    }
}