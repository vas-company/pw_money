﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PW.Migration
{
    public static class TestData
    {
        static JArray users=null;
        static  TestData()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(),"data.json");
            if(File.Exists(path))
            {
                users = JArray.Parse(File.ReadAllText(path));
            }
        }
        public static JArray Users
        {
            get
            {
                return users;
            }
        }
    }
}