﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json.Linq;
using PW.DAL.Identity;
using PW.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace PW.Migration
{
    public class MigrationConfiguration : DbMigrationsConfiguration<AppContext>
    {
        public MigrationConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(AppContext context)
        {
            if (context.Users.Any() || context.Operations.Any())
            {
                return;
            }

            this.SeedUsers(context);
        }
        
        private void SeedUsers(AppContext context)
        {
            var usersJson = TestData.Users;
            if (usersJson == null) return;
            var users = new List<User>();
            var userManager = AuthUserManager.Create(context);
            foreach (JToken item in usersJson)
            {
                var user = new User
                {
                    UserName = item["name"].ToString(),
                    Email = item["email"].ToString(),
                    Balance = (decimal)item["balance"]
                };
                var userCreateResult = userManager.Create(user, "123456");
                users.Add(user);
            }
            var senderUser = users[0];
            var recipientUser = users[1];
            var operation = new Operation()
            {
                Sender = senderUser,
                SenderBalance = senderUser.Balance,
                Recipient = recipientUser,
                RecipientBalance = recipientUser.Balance,
                Amount = 200,
                Date = DateTime.Now
            };
            context.Operations.Add(operation);
            context.SaveChanges();
        }
    }
}