﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Owin.Testing;
using PW.App_Start;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Net;
using Microsoft.AspNet.Identity.Owin;
using PW.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;

namespace PW.API.Test
{
    [TestClass]
    public class UserControllerTest : BaseControllerTest
    {
        private string name = "User1";
        private string password = "123456";

        private string controller = "/api/user";

        [TestMethod]
        public async Task Create()
        {
            using (var server = TestServer.Create<Startup>())
            {
                string uniq = DateTime.Now.Ticks.ToString();
                var dictParam = new Dictionary<string, string>
                {
                    { "UserName", $"TestUser  {uniq}"},
                    { "Email", $"{uniq}@test.ru" },
                    { "Password", "123456" }
                };
                var param = GetParam(dictParam);
                var result = await server
                    .CreateRequest(controller + "?" + param)
                    .AddHeader("Accept", "application/json")
                    .AddHeader("Content-Type", "application/json")
                    .PostAsync();
                Assert.AreEqual(result.StatusCode, HttpStatusCode.OK, "Создание пользователя");

            }

        }

        [TestMethod]
        public async Task GetAllUsers()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var token = await GetAccessToken(server, name, password);
                var result = await server
                    .CreateRequest(controller)
                    .AddHeader("Accept", "application/json")
                    .AddHeader("Content-Type", "application/json")
                    .AddHeader("Authorization", $"Bearer {token}")
                    .GetAsync();
                Assert.AreEqual(result.StatusCode, HttpStatusCode.OK, "Список пользователей");
            }
        }

        [TestMethod]
        public async Task GetBalance()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var token = await GetAccessToken(server, name, password);
                var result = await server
                    .CreateRequest($"{controller}/balance")
                    .AddHeader("Accept", "application/json")
                    .AddHeader("Content-Type", "application/json")
                    .AddHeader("Authorization", $"Bearer {token}")
                    .GetAsync();
                Assert.AreEqual(result.StatusCode, HttpStatusCode.OK, "Список пользователей");
            }
        }
    }
}
