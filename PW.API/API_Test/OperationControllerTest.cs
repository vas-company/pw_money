﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PW.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace PW.API.Test
{
    [TestClass]
    public class OperationControllerTest : BaseControllerTest
    {
        private string name = "User1";
        private string password = "123456";
        private string controller = "/api/operation";

        [TestMethod]
        public async Task Create_Ok()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var dictParam = new Dictionary<string, string>
                {
                    { "Amount", "2" },
                    { "RecipientName", "User2" }
                };
                var param = GetParam(dictParam);
                var token = await GetAccessToken(server, name, password);
                var result = await server
                    .CreateRequest(controller + "?" + param)
                    .AddHeader("Accept", "application/json")
                    .AddHeader("Content-Type", "application/json")
                    .AddHeader("Authorization", $"Bearer {token}")
                    .PostAsync();
                Assert.AreEqual(result.StatusCode, HttpStatusCode.OK, "Создание транзакции");
            }
        }

        [TestMethod]
        public async Task Create_Amount_Bad()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var dictParam = new Dictionary<string, string>
                {
                    { "Amount", decimal.MaxValue.ToString() },
                    { "RecipientName", "User2" }
                };
                var param = GetParam(dictParam);
                var token = await GetAccessToken(server, name, password);
                var result = await server
                    .CreateRequest(controller + "?" + param)
                    .AddHeader("Accept", "application/json")
                    .AddHeader("Content-Type", "application/json")
                    .AddHeader("Authorization", $"Bearer {token}")
                    .PostAsync();
                Assert.AreEqual(result.StatusCode, HttpStatusCode.BadRequest, "Создание транзакции с неправильным amount");
            }
        }

        [TestMethod]
        public async Task GetHistory()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var token = await GetAccessToken(server, name, password);
                var result = await server
                    .CreateRequest(controller)
                    .AddHeader("Accept", "application/json")
                    .AddHeader("Content-Type", "application/json")
                    .AddHeader("Authorization", $"Bearer {token}")
                    .GetAsync();
                Assert.AreEqual(result.StatusCode, HttpStatusCode.OK, "История операций");
            }
        }
    }
}