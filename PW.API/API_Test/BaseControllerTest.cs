﻿using Microsoft.Owin.Testing;
using Newtonsoft.Json.Linq;
using PW.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PW.API.Test
{
    //TODO: тесты реализованы только для проверки без клиента, это не true тесты
    public class BaseControllerTest
    {
        protected virtual async Task<string> GetAccessToken(TestServer server, string username, string password)
        {
            string token = string.Empty;
            var result = await server.CreateRequest(Startup.TokenEndpointPath).And(x => x.Content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("grant_type", "password")
            })).PostAsync();
            if (result.StatusCode == HttpStatusCode.OK)
            {
                var responseString = await result.Content.ReadAsStringAsync();
                var responseJson = JObject.Parse(responseString);
                token = responseJson["access_token"]?.ToString();
            }
            return token;
        }

        protected virtual string GetParam(Dictionary<string, string> dictParam)
        {
            var param = new StringBuilder();
            foreach (var item in dictParam)
            {
                if (param.Length > 0)
                {
                    param.Append("&");
                }
                param.Append(item.Key + "=" + item.Value);
            }
            return param.ToString();
        }

    }
}