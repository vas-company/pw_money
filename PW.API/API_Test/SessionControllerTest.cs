﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PW.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace PW.API.Test
{
    [TestClass]
    public class SessionControllerTest : BaseControllerTest
    {
        private string name = "User1";
        private string password = "123456";

        private string controller = "/api/session";

        [TestMethod]
        public async Task Token_Deactivate()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var token = await GetAccessToken(server, name, password);

                var firstResult = await server
                    .CreateRequest(controller)
                    .AddHeader("Accept", "application/json")
                    .AddHeader("Content-Type", "application/json")
                    .AddHeader("Authorization", $"Bearer {token}")
                    .SendAsync("DELETE");
                Assert.AreEqual(firstResult.StatusCode, HttpStatusCode.OK, "Удаление сессии");

                var secondResult = await server
                   .CreateRequest(controller)
                   .AddHeader("Accept", "application/json")
                   .AddHeader("Content-Type", "application/json")
                   .AddHeader("Authorization", $"Bearer {token}")
                   .SendAsync("DELETE");
                Assert.AreEqual(secondResult.StatusCode, HttpStatusCode.Unauthorized, "Повторная попытка удаления сессии");
            }

        }
    }
}