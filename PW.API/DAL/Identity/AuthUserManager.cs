﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using PW.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.DAL.Identity
{
    public class AuthUserManager : UserManager<User>
    {
        public AuthUserManager(IUserStore<User> store)
            : base(store)
        {
        }

        public static AuthUserManager Create(IdentityFactoryOptions<AuthUserManager> options, IOwinContext context)
        {
            var manager = new AuthUserManager(new UserStore<User>(context.Get<AppContext>()));
            ManagerConfigurate(ref manager);
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<User>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

        public static AuthUserManager Create(AppContext context)
        {
            var manager = new AuthUserManager(new UserStore<User>(context));
            ManagerConfigurate(ref manager);
            return manager;
        }

        private static void ManagerConfigurate(ref AuthUserManager manager)
        {
            // Настройка логики проверки имен пользователей
            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Настройка логики проверки паролей
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6
            };
        }
    }
}