﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PW.DAL.Identity
{
    public class OAuthBearerProvider : OAuthBearerAuthenticationProvider
    {
        public override Task ValidateIdentity(OAuthValidateIdentityContext context)
        {
            //TODO: найти другие методы деактивации токена или изменить подход к logout
            var userManager = context.OwinContext.GetUserManager<AuthUserManager>();
            var user = userManager.FindById(context.Ticket.Identity.GetUserId());
            var claims = context.Ticket.Identity.Claims;
            var claimStamp = claims.FirstOrDefault(claim => claim.Type == "AspNet.Identity.SecurityStamp");
            if (user== null || claimStamp == null || !claimStamp.Value.Equals(user.SecurityStamp))
            {
                context.Rejected();
                return Task.FromResult<object>(null);
            }
            else
            {
                return base.ValidateIdentity(context);
            }
        }
    }
}