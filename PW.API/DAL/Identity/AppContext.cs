﻿using Microsoft.AspNet.Identity.EntityFramework;
using PW.DAL.Models;
using PW.Migration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PW.DAL.Identity
{
    public class AppContext : IdentityDbContext<User>
    {
        public AppContext()
            : base("DefaultContext", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppContext, MigrationConfiguration>());
        }

        public static AppContext Create()
        {
            return new AppContext();
        }

        public DbSet<Operation> Operations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new OperationConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}