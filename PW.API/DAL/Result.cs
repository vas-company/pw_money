﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.DAL
{
    public class Result<TResult>: Result
    {
        public TResult Value { get; set; }

        public new Result<TResult> Fail(IEnumerable<string> errors)
        {
            Succeeded = false;
            Errors.AddRange(errors);
            return this;
        }

        public new Result<TResult> Fail(string error)
        {
            Succeeded = false;
            Errors.Add(error);
            return this;
        }

        public Result<TResult> Success(TResult value)
        {
            Succeeded = true;
            Value = value;
            return this;
        }
    }

    public class Result: IResultError
    {
        public bool Succeeded { get; set; } = true;
        public List<string> Errors { get; private set; } = new List<string>();
        public Result(){}
        public Result(IEnumerable<string> errors)
        {
            Fail(errors);
        }

        public Result Fail(IEnumerable<string> errors)
        {
            Succeeded = false;
            Errors.AddRange(errors);
            return this;
        }

        public Result Fail(string error)
        {
            Succeeded = false;
            Errors.Add(error);
            return this;
        }

        public Result Success()
        {
            Succeeded = true;
            return this;
        }
    }

    public interface IResultError
    {
        List<string> Errors { get; }
        bool Succeeded { get; set; }
    }
}