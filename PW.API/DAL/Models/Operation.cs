﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PW.DAL.Models
{
    public class Operation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public decimal SenderBalance { get; set; }
        public decimal RecipientBalance { get; set; }
        public string SenderId { get; set; }
        public virtual User Sender { get; set; }
        public string RecipientId { get; set; }
        public virtual User Recipient { get; set; }
        public DateTime Date { get; set; }
        [Required]
        public decimal Amount { get; set; }

    }
}