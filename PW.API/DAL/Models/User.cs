﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PW.DAL.Models
{
    public class User: IdentityUser
    {
        [Required]
        public decimal Balance { get; set; }
        public virtual ICollection<Operation> Operations { get; set; }
    }
}