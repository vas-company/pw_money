﻿using PW.DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.DAL
{
    public class Repository
    {
        public Repository(AppContext context, AuthUserManager userManager)
        {
            this.Context = context;
            this.UserManager = userManager;
        }

        public AuthUserManager UserManager { get; private set; }
        public AppContext Context { get; private set; }
    }
}