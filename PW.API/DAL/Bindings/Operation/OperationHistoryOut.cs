﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.DAL.Bindings
{
    public class OperationHistoryOut
    {
        public decimal Balance { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string RecipientName { get; set; }
        public string RecipientEmail { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public bool IsSender { get; set; }
    }
}