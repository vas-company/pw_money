﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PW.DAL.Bindings
{
    public class OperationCreateIn
    {
        [Required]
        [Display(Name = "Сумма")]
        [Range(0, double.MaxValue)]
        public decimal Amount { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Получатель")]
        public string RecipientName { get; set; }
    }
}