﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PW.DAL.Bindings
{
    public class UserCreateIn
    {
        [Required(AllowEmptyStrings =false)]
        [Display(Name ="Имя")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Пароль")]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Почта")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}