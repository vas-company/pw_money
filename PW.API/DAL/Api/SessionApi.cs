﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PW.DAL
{
    public class SessionApi
    {
        #region Init
        private Repository Repository;
        private Api API;

        public SessionApi(Api api, Repository repository)
        {
            this.Repository = repository;
            this.API = api;
        }
        #endregion

        public async Task<Result> UpdateSecurityStampAsync(string userId)
        {
            var resultIdentity = await Repository.UserManager.UpdateSecurityStampAsync(userId);
            var result = (resultIdentity.Succeeded) ? new Result() : new Result(resultIdentity.Errors);
            return result;
        }
    }
}