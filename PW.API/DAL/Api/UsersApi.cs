﻿using Microsoft.AspNet.Identity;
using PW.DAL.Bindings;
using PW.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PW.DAL
{
    public class UsersApi
    {
        #region Init
        private Repository Repository;
        private Api API;

        public UsersApi(Api api, Repository repository)
        {
            this.Repository = repository;
            this.API = api;
        }
        #endregion

        public async Task<Result<object>> GetBalanceAsync(string userId)
        {
            var result = new Result<object>();
            var user = await Repository.UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                return result.Fail("Пользователь не найден");
            }
            var balance = new { user.Balance };
            return result.Success(balance);
        }

        public async Task<Result<object>> GetAllAsync()
        {
            return await Task.Factory.StartNew(() =>
            {
                var result = new Result<object>();
                //TODO: возможна нужна фильтрация по совпадению имени
                var users = Repository.UserManager.Users.Select(s => new { s.UserName, s.Email });
                if (users.Count() <= 0)
                {
                    return result.Fail("Пользователи не найдены");
                }
                return result.Success(users);
            });
        }

        public async Task<Result> CreateAsync(UserCreateIn model)
        {
            var user = new User
            {
                UserName = model.UserName,
                Email = model.Email,
                Balance = 500
            };
            var resultIdentity = await Repository.UserManager.CreateAsync(user, model.Password);
            var result = (resultIdentity.Succeeded) ? new Result() : new Result(resultIdentity.Errors);
            return result;
        }
    }
}