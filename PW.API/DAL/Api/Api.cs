﻿using DAL;
using PW.DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PW.DAL
{
    public class Api
    {
        public Api(Repository repository)
        {
            this.Operation = new OperationApi(this,repository);
            this.Users = new UsersApi(this, repository);
            this.Session = new SessionApi(this, repository);
        }

        public OperationApi Operation { get; private set; }
        public SessionApi Session { get; private set; }
        public UsersApi Users { get; private set; }
    }

    
}