﻿using PW.DAL;
using PW.DAL.Bindings;
using PW.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DAL
{
    public class OperationApi
    {
        #region Init
        private Repository Repository;
        private Api API;

        public OperationApi(Api api, Repository repository)
        {
            this.Repository = repository;
            this.API = api;
        }
        #endregion
        public async Task<Result> CreateAsync(string userId, OperationCreateIn model)
        {
            var result = new Result();
            var senderUser = await Repository.UserManager.FindByIdAsync(userId);
            var recipientUser = await Repository.UserManager.FindByNameAsync(model.RecipientName);
            if (recipientUser == null)
            {
                return result.Fail($"Пользователь {model.RecipientName} не найден");
            }
            if(senderUser.UserName==recipientUser.UserName)
            {
                return result.Fail($"Перевод себе не возможен");
            }
            if (senderUser.Balance < model.Amount)
            {
                return result.Fail("Недостаточно средств на счету");
            }
            senderUser.Balance = senderUser.Balance - model.Amount;
            recipientUser.Balance = recipientUser.Balance + model.Amount;
            var operation = new Operation()
            {
                Sender = senderUser,
                SenderBalance = senderUser.Balance,
                Recipient = recipientUser,
                RecipientBalance = recipientUser.Balance,
                Amount = model.Amount,
                Date = DateTime.Now
            };
            Repository.Context.Operations.Add(operation);
            var count = await Repository.Context.SaveChangesAsync();
            return result.Success();
        }

        public async Task<Result<List<OperationHistoryOut>>> FindByIdAsync(string userId)
        {
            return await Task.Factory.StartNew(() =>
            {
                var result = new Result<List<OperationHistoryOut>>();
                var operationsDB = Repository.Context.Operations.Where(o => o.RecipientId == userId || o.SenderId == userId);
                int count = operationsDB.Count();
                if (count <= 0)
                {
                    return result.Fail("Операции не найдены");
                }
                var operations = new List<OperationHistoryOut>(count);
                foreach (var item in operationsDB)
                {
                    var o = new OperationHistoryOut()
                    {
                        Amount = item.Amount,
                        Date = item.Date,
                        RecipientName = item.Recipient.UserName,
                        RecipientEmail = item.Recipient.Email,
                        SenderName = item.Sender.UserName,
                        SenderEmail = item.Sender.Email,
                    };
                    //нельзя передать чужой баланс пользователю
                    if (item.SenderId == userId)
                    {
                        o.IsSender = true;
                        o.Balance = item.SenderBalance;
                    }
                    else
                    {
                        o.IsSender = false;
                        o.Balance = item.RecipientBalance;
                    }
                    operations.Add(o);
                }
                return result.Success(operations);
            });
        }
    }
}